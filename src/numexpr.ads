package numexpr is
    type OperatorType is
        (add,subtract,multiply,divide);

    subtype values_type is float ;
    type ExpressionType( terminal : boolean := true );
    type ExpressionPtrType is access ExpressionType;
    type ExpressionType ( terminal : boolean := true ) is record
        case terminal is
            when true =>
                Value : Values_Type;
            when false =>
                Oper : OperatorType;
                left : ExpressionPtrType ;
                right : ExpressionPtrType ;
        end case ;
    end record;

    function Generate ( complexity : Integer ) return ExpressionPtrType;
    function Evaluate( expr : ExpressionPtrType) return Values_Type;
    function Image( expr : ExpressionPtrType ) return String;
    function Image( val : values_type ) return String ;

    procedure Test;
end numexpr ;