with Interfaces.C; use Interfaces.C;

package body stdio is


  procedure printf(fmtstr : Interfaces.C.char_array ;
                    intval : Integer );
   pragma Import(C,printf);
   function snprintf( buffer : System.Address ;
                      buflen : Interfaces.C.int;
                      fmtstr : Interfaces.C.char_array ;
                      intval : Interfaces.C.int ) return Interfaces.C.int ;
   pragma Import(C,snprintf);

   function snprintf_f( buffer : System.Address ;
                      buflen : Interfaces.C.int;
                      fmtstr : Interfaces.C.char_array ;
                      fval : Interfaces.C.double ) return Interfaces.C.int ;
   pragma Import(C,snprintf_f,"snprintf");

    buffer : String(1..128) ;
    buflen : Interfaces.C.Int ;

   function Image(val : Integer; format : String := "%d") return String is
      fmtstr : Interfaces.C.char_array := To_C(format);
   begin
      buflen := snprintf(buffer'Address, buffer'Length, fmtstr, Interfaces.C.int(val));
      return buffer(1..Integer(buflen));
   end Image ;

   function Image(val : Float ; format : String := "%f") return String is
        fmtstr : Interfaces.C.char_array := To_C(format);
   begin
        buflen := snprintf_f(buffer'Address, buffer'Length, fmtstr, Interfaces.C.double(val));
        return buffer(1..Integer(buflen));
   end Image ;

end stdio;