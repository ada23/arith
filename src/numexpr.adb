with Ada.Numerics.Float_Random;
with Ada.Numerics.Discrete_Random;

with Ada.Text_Io; use Ada.Text_Io;
with Ada.Float_Text_Io; use Ada.Float_Text_Io;
with stdio;

with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Interfaces.C; use Interfaces.c;
with Interfaces.C.Strings; use Interfaces.C.Strings;
package body numexpr is
    package R renames Ada.Numerics.Float_Random ;
    Gd : R.generator ;

    type Two_Digits is new Integer range 0..99;
    package DR is new Ada.Numerics.Discrete_Random(Two_Digits);
    Gdr : DR.Generator;
    
    function Generate ( complexity : Integer ) return ExpressionPtrType is
        result : ExpressionPtrType ; 
        temp : Two_Digits := DR.Random(Gdr);
        lowercomplexity : Integer := complexity - 1;
    begin
       if complexity = 0 or temp < 10
        then
            result := new ExpressionType(true);
            result.Value := Values_Type(100.0 * R.Random(Gd)) ;
            Put_Line(Image(result.Value));
            return result;
        end if ;
        temp := DR.Random(Gdr) ;
        case temp is
            when 0..24 =>
                begin 
                    result := new ExpressionType (false);
                    result.Oper := add ;
                end ;
            when 25..49 =>
                begin 
                    result := new ExpressionType (false);
                    result.Oper := subtract ;
                end ;
            when 50..74 => 
                begin 
                    result := new ExpressionType (false);
                    result.Oper := multiply ;
                end ;
            when 75..99 => null;
                begin 
                    result := new ExpressionType (false);
                    result.Oper := divide ;
                end ;
        end case ;
        result.left := Generate(lowercomplexity);
        result.right := Generate(lowercomplexity);
        return result;
    end Generate;

    function Evaluate( expr : ExpressionPtrType) return Values_Type is
        result : Values_Type := 0.0;
    begin
        if expr.terminal
        then
            return expr.Value ;
        end if ;
        case expr.Oper is
            when add => return Evaluate(expr.left) + Evaluate(expr.right) ;
            when subtract => return Evaluate(expr.left) - Evaluate(expr.right) ;
            when multiply => return Evaluate(expr.left) * Evaluate(expr.right) ;
            when divide =>
                if Evaluate(expr.right) > 0.01
                then 
                    return Evaluate(expr.left) / Evaluate(expr.right) ;
                else
                    return 0.0;
                end if ;
        end case;
    end Evaluate;

    function Image( val : values_type ) return string is
        result : String(1..10) := (others => ' ');
    begin
        Put(item => val, to=>result, aft => 2 , exp => 0);
        return result;
    end Image;

    function Image( expr : ExpressionPtrType ) return String is
        function Image( left,right : ExpressionPtrType; connector : String ) return String is
            leftImage : Unbounded_String;
            rightImage : Unbounded_String;
        begin
            leftImage := "(" & To_Unbounded_String( Image(left) ) & ")" ;
            rightImage := "(" & To_Unbounded_String( Image(right) ) & ")" ;
            return To_String(leftImage) & connector & To_String(rightImage) ;
        end Image;
    begin
        if expr.terminal
        then
            return Image(expr.Value);
        end if ;
        case expr.Oper is
            when add =>
                return Image(expr.left) & " + " & Image(expr.right) ;
            when subtract =>
                return Image(expr.left) & " - " & Image(expr.right) ;
            when multiply =>
                --Put_Line( "(" & Image(expr.left) & ") * (" & Image(expr.right) & ")" ) ;
                return Image(expr.left,expr.right," * ");
            when divide =>
                --Put_Line( "(" & Image(expr.left) & ") / (" & Image(expr.right) & ")" ) ;
                return Image(expr.left,expr.right," / ");
        end case ;
    end Image;
    procedure Test is
        x : ExpressionPtrType ;
    begin
        x := Generate(3);
        Put_Line( Image(x) );
    end Test ;
begin
    R.Reset(Gd) ;
    Dr.Reset(Gdr);
end numexpr;