with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Float_Text_Io; use Ada.Float_Text_Io;
with numexpr ; use numexpr;

procedure Arith is
   cplx : Integer := 2;
   expr : ExpressionPtrType;
begin
   if ARGUMENT_COUNT >= 1
   then
      cplx := Integer'Value(Argument(1));
   end if ;
   expr := Generate(cplx);
   Put("Problem : ");
   Put_Line(Image(expr));
   Put("Solution : "); Put(numexpr.Image(Evaluate(expr))); New_Line;
end Arith;
