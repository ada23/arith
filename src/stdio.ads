with System;
with Interfaces.C;

package Stdio is

   function Image(val : Integer; format : String := "%d") return String ;
   function Image(val : Float ; format : String := "%f") return String ;
  
end Stdio;